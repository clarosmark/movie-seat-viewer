package com.mobile.app.movieviewer.di

import com.google.gson.Gson
import com.google.gson.JsonParseException
import okhttp3.Interceptor
import okhttp3.MediaType
import okio.IOException
import timber.log.Timber


class ErrorInterceptor constructor(
    private val gson: Gson
) : Interceptor {

    private val MSG_ERROR_JSON = "Could not parse JSON response, ignoring"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val request = chain.request()
        val response = chain.proceed(request)

        val responseBody = response.body
        val responseCode = response.code

        // Only intercept JSON type responses and ignore the rest.
        if (responseBody != null) {
            val contentType: MediaType? = responseBody.contentType()
            if (contentType != null && contentType.subtype.toLowerCase() == "json") {
                try {
//                    val source: BufferedSource = responseBody.source()
//                    source.request(Long.MAX_VALUE) // Buffer the entire body.
//                    val buffer = source.buffer
//                    val charset = contentType.charset(StandardCharsets.UTF_8)
//                    // Clone the existing buffer is they can only read once so we still want to pass the original one to the chain.
//                    val json = buffer.clone().readString(charset!!)
//                    val type =
//                        object : TypeToken<GenericResponse<Map<String?, String?>?>?>() {}.type
//                    errorMapper.throwIfError(gson.fromJson(json, type))
                    if (responseCode != 200){
                        throw ErrorNetworkException(responseBody.string())
                    }

                } catch (e: JsonParseException) {
                    e.printStackTrace()
                    Timber.d(MSG_ERROR_JSON + e.message)
                }
            }
        }

        return response
    }


}