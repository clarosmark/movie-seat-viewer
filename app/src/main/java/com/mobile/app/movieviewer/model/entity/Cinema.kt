package com.mobile.app.movieviewer.model.entity

data class Cinema(
    val cinemas: List<CinemaX>,
    val parent: String
)