package com.mobile.app.movieviewer.model.entity

data class MovieEntity(
    val advisory_rating: String,
    val average_rating: Any,
    val canonical_title: String,
    val cast: List<String>,
    val genre: String,
    val has_schedules: Int,
    val is_featured: Int,
    val is_inactive: Int,
    val is_showing: Int,
    val link_name: String,
    val movie_id: String,
    val order: Int,
    val poster: String,
    val poster_landscape: String,
    val ratings: Ratings,
    val release_date: String,
    val runtime_mins: String,
    val synopsis: String,
    val theater: String,
    val total_reviews: Any,
    val trailer: String,
    val variants: List<String>,
    val watch_list: Boolean,
    val your_rating: Int
)