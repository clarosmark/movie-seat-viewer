package com.mobile.app.movieviewer.model.entity

data class Date(
    val date: String,
    val id: String,
    val label: String
)