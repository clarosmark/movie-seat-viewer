package com.mobile.app.movieviewer.model.entity

data class ScheduleEntity(
    val cinemas: List<Cinema>,
    val dates: List<Date>,
    val times: List<Time>
)