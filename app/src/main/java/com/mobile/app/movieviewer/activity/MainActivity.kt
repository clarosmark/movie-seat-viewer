package com.mobile.app.movieviewer.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.mobile.app.movieviewer.R
import com.mobile.app.movieviewer.databinding.ActivityMainBinding
import com.mobile.app.movieviewer.model.stateflow.UIMovieStateFlow
import com.mobile.app.movieviewer.viewmodel.MainActivityViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    companion object {
        const val Theater = "Theater"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launchWhenStarted {
            viewModel.movieStateFlow.collect { stateFlow ->
                when (stateFlow) {
                    is UIMovieStateFlow.Loading -> {
                        binding.formContainer.visibility =
                            if (stateFlow.isLoading) View.GONE else View.VISIBLE
                        binding.progressBar.visibility =
                            if (stateFlow.isLoading) View.VISIBLE else View.GONE
                    }
                    is UIMovieStateFlow.Success -> {
                        with(binding) {
                            tvName.text = stateFlow.movieDetails.movieName
                            tvGenre.text = stateFlow.movieDetails.movieGenre
                            tvDuration.text = stateFlow.movieDetails.movieDuration
                            tvAdvisoryRating.text = stateFlow.movieDetails.movieRating
                            tvReleaseDate.text = stateFlow.movieDetails.movieReleasedDate
                            tvSynopsis.text = stateFlow.movieDetails.movieSynopsis

                            Picasso.get()
                                .load(stateFlow.movieDetails.imgLandscapeURL)
                                .placeholder(R.drawable.ic_launcher_background)
                                .error(R.drawable.ic_launcher_background)
                                .fit()
                                .into(binding.imgMovieLandscape)

                            Picasso.get()
                                .load(stateFlow.movieDetails.imgPortraitURL)
                                .placeholder(R.drawable.ic_launcher_background)
                                .error(R.drawable.ic_launcher_background)
                                .fit()
                                .into(binding.imgMoviePortrait)

                            btnSubmit.setOnClickListener {
                                val intent =
                                    Intent(applicationContext, MovieDetailsActivity::class.java).apply {
                                        putExtra(Theater, stateFlow.movieDetails.theater)
                                    }
                                startActivity(intent)
                            }
                        }

                    }

                    is UIMovieStateFlow.Error -> {
                        showErrorDialog()
                    }
                    is UIMovieStateFlow.Empty -> {

                    }
                }
            }
        }
    }

    private fun showErrorDialog() {
        if (isFinishing) {
            return
        }
        val builder = AlertDialog.Builder(this)
            .setTitle("Error Request")
            .setMessage("Sorry, you have error. Please try again")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                finish()
            }
            .setCancelable(false)
            .create()
        builder.show()
    }


}