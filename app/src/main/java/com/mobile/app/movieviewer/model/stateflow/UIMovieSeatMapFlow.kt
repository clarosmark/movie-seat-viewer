package com.mobile.app.movieviewer.model.stateflow

import com.mobile.app.movieviewer.model.model.MovieDetails

sealed class UIMovieSeatMapFlow {
    data class Error(val title: String, val errorMessage: String) : UIMovieSeatMapFlow()
    data class Loading(val isLoading: Boolean) : UIMovieSeatMapFlow()
    object Empty : UIMovieSeatMapFlow()
}
