package com.mobile.app.movieviewer.usecase

import com.mobile.app.movieviewer.model.entity.MovieEntity
import com.mobile.app.movieviewer.model.model.MovieDetails
import com.mobile.app.movieviewer.usecase.util.DateUtil
import javax.inject.Inject

class MovieDetailsMapper @Inject constructor(
    private val dateUtil: DateUtil
) {

    fun mapFromEntity(movieEntity: MovieEntity): MovieDetails {
        val releasedDate = dateUtil.prettifyDate(dateUtil.formatDate(movieEntity.release_date)) ?: ""
        val duration = dateUtil.convertMinuteToHours(movieEntity.runtime_mins)
        return MovieDetails(
            theater = movieEntity.theater,
            movieDuration = duration,
            movieGenre = movieEntity.genre,
            movieName = movieEntity.canonical_title,
            movieRating = movieEntity.advisory_rating,
            movieReleasedDate = releasedDate,
            movieSynopsis = movieEntity.synopsis,
            imgLandscapeURL = movieEntity.poster_landscape,
            imgPortraitURL = movieEntity.poster
        )
    }

}