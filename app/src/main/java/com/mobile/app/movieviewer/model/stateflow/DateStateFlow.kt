package com.mobile.app.movieviewer.model.stateflow

import com.mobile.app.movieviewer.model.model.MovieDetails

sealed class DateStateFlow{
    data class DateSchedule(val dateScheduleList: List<String>) : DateStateFlow()
//    data class CinemaValue(val cinemaList: List<String>) : DateStateFlow()
//    data class TimeValue(val timeList: List<String>) : DateStateFlow()
//    data class CreateRowSeat(val movieDetails: MovieDetails) : DateStateFlow()
}
