package com.mobile.app.movieviewer.usecase.util

import timber.log.Timber
import java.math.BigDecimal
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    private val YYYY_MM_DD = "yyyy-MM-dd"
    private val MMM_DD_YYYY = "MMM dd, yyyy"

    fun formatDate(stringDate: String) : Date {
        try {
            return SimpleDateFormat(YYYY_MM_DD, Locale.getDefault()).parse(stringDate)
        } catch (e: ParseException) {
            Timber.e(e)
        }
        return Calendar.getInstance().time
    }

    fun prettifyDate(date: Date): String? {
        return SimpleDateFormat(MMM_DD_YYYY, Locale.getDefault()).format(date)
    }

    fun convertMinuteToHours( stringDuration: String) : String{
        var returnDuration = ""
        try {
            val duration = BigDecimal(stringDuration).toInt()
            val hour = duration / 60
            val minute = duration %  60

            if (hour > 0){
                returnDuration = "$hour hr "
            }
            if (minute > 1){
                returnDuration.plus("$minute minutes")
            }
            else if (minute in 1..1){
                returnDuration.plus("$minute minute")
            }
        }
        catch (e: NumberFormatException){
            Timber.e(e)
        }
        return returnDuration
    }


}