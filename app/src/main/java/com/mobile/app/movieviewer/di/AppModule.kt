package com.mobile.app.movieviewer.di

import android.content.Context
import android.net.Uri
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mobile.app.movieviewer.MovieViewer
import com.mobile.app.movieviewer.api.MovieService
import com.mobile.app.movieviewer.usecase.util.DateUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val SCHEME: String = "http"
    private const val AUTHORITY: String = "ec2-52-76-75-52.ap-southeast-1.compute.amazonaws.com"

    private val BASE_URI = Uri.Builder()
        .scheme(SCHEME)
        .encodedAuthority(AUTHORITY)
        .build()

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext context: Context): MovieViewer {
        return context as MovieViewer
    }

    @Singleton
    @Provides
    fun provideOkHttpBuilder(interceptor: HttpLoggingInterceptor): OkHttpClient.Builder {
        val specs: ConnectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            .tlsVersions(TlsVersion.TLS_1_2)
            .build()
        val specClearText = ConnectionSpec.Builder(ConnectionSpec.CLEARTEXT)
            .build()

        //        applyCertificatePinner(
//            builder,
//            BuildConfig.API_CA_PINS,
//            BuildConfig.API_CA_PATTERN
//        )

        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .connectionSpecs(listOf(specs,specClearText))
    }


    @Singleton
    @Provides
    fun provideRetrofit(
        gson: Gson, okHttpBuilder: OkHttpClient.Builder,
        interceptor: HttpLoggingInterceptor,
//        errorInterceptor: ErrorInterceptor
    ): Retrofit {
        val builder = okHttpBuilder.addInterceptor(interceptor)
//            .addInterceptor(errorInterceptor)
        return Retrofit.Builder()
            .baseUrl(BASE_URI.toString())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(builder.build())
            .build()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(
            HttpLoggingInterceptor.Level.BODY
        )
    }

    @Singleton
    @Provides
    fun provideMovieService(retrofit: Retrofit): MovieService {
        return retrofit.create(MovieService::class.java)
    }

    @Provides
    fun provideDateUtil(): DateUtil = DateUtil

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
//            .setExclusionStrategies(object : ExclusionStrategy {
//                override fun shouldSkipField(f: FieldAttributes): Boolean {
//                    return f.getAnnotation(SkipSerialisation::class.java) != null
//                }
//
//                override fun shouldSkipClass(clazz: Class<*>?): Boolean {
//                    return false
//                }
//            })
            .setLenient()
            .create()
    }

}