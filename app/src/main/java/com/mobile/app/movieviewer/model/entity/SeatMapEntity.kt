package com.mobile.app.movieviewer.model.entity

import com.google.gson.annotations.SerializedName

data class SeatMapEntity(
    val available: Available,
    @SerializedName("seatmap")
    val seatMap: List<List<String>>
)