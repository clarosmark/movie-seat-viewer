package com.mobile.app.movieviewer.model.entity

data class Time(
    val parent: String,
    val times: List<TimeX>
)