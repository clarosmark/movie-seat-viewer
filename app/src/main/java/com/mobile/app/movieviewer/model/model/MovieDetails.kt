package com.mobile.app.movieviewer.model.model

data class MovieDetails(
    val theater: String,
    val movieName: String,
    val movieGenre: String,
    val movieRating: String,
    val movieDuration: String,
    val movieReleasedDate : String,
    val movieSynopsis: String,
    val imgPortraitURL : String,
    val imgLandscapeURL: String
)