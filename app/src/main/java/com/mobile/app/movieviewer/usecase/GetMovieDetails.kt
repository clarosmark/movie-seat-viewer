package com.mobile.app.movieviewer.usecase

import com.mobile.app.movieviewer.api.MovieService
import com.mobile.app.movieviewer.model.entity.MovieEntity
import com.mobile.app.movieviewer.model.entity.ScheduleEntity
import com.mobile.app.movieviewer.model.entity.SeatMapEntity
import javax.inject.Inject

class GetMovieDetails @Inject constructor(
    private val movieService: MovieService
) {

    suspend fun getMovieDetails() : MovieEntity{
        return movieService.getMovie()
    }

    suspend fun getMovieSchedules() : ScheduleEntity{
        return movieService.getSchedules()
    }

    suspend fun getMovieSeatMap() : SeatMapEntity{
        return movieService.getSeatMaps()
    }
}