package com.mobile.app.movieviewer.api

import com.mobile.app.movieviewer.model.entity.MovieEntity
import com.mobile.app.movieviewer.model.entity.ScheduleEntity
import com.mobile.app.movieviewer.model.entity.SeatMapEntity
import retrofit2.http.GET

interface MovieService {

    @GET("/movie.json")
    suspend fun getMovie(): MovieEntity

    @GET("/schedule.json")
    suspend fun getSchedules(): ScheduleEntity

    @GET("/seatmap.json")
    suspend fun getSeatMaps(): SeatMapEntity
}