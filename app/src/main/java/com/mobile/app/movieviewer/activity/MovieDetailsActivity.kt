package com.mobile.app.movieviewer.activity

import android.R
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.mobile.app.movieviewer.databinding.ActivityMovieDetailsBinding
import com.mobile.app.movieviewer.model.stateflow.DateStateFlow
import com.mobile.app.movieviewer.model.stateflow.UIMovieSeatMapFlow
import com.mobile.app.movieviewer.model.stateflow.UIMovieStateFlow
import com.mobile.app.movieviewer.viewmodel.MovieDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import timber.log.Timber


@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity() {

    private val viewModel: MovieDetailsViewModel by viewModels()
    private lateinit var binding: ActivityMovieDetailsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launchWhenStarted {
            viewModel.seatMapFlow.collect { stateFlow ->
                when(stateFlow){
                    is UIMovieSeatMapFlow.Loading -> {
                        binding.formContainer.visibility =
                            if (stateFlow.isLoading) View.GONE else View.VISIBLE
                        binding.progressBar.visibility =
                            if (stateFlow.isLoading) View.VISIBLE else View.GONE
                    }
                    else ->{
                       Timber.e( stateFlow.toString())
                    }
                }
            }

            viewModel.dateStateFlow.collect{ stateFlow ->
                when(stateFlow){
                    is DateStateFlow.DateSchedule -> {
                        val data = stateFlow.dateScheduleList
                        Timber.e("data: "+Gson().toJson(data))
                        val dataAdapter = ArrayAdapter(
                            applicationContext,
                            R.layout.simple_spinner_item,
                            data
                        )
                        binding.pickerDate.adapter = dataAdapter
                        binding.pickerDate.prompt = "Tesst"
                    }
                }
            }
        }


        binding.pickerCinema.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

    }
}