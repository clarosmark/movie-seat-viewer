package com.mobile.app.movieviewer.model.entity

data class TimeX(
    val id: String,
    val label: String,
    val popcorn_label: String,
    val popcorn_price: String,
    val price: String,
    val schedule_id: String,
    val seating_type: String,
    val variant: Any
)