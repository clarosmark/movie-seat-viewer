package com.mobile.app.movieviewer.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobile.app.movieviewer.model.stateflow.UIMovieStateFlow
import com.mobile.app.movieviewer.usecase.GetMovieDetails
import com.mobile.app.movieviewer.usecase.MovieDetailsMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val getMovieDetails: GetMovieDetails,
    private val movieDetailsMapper: MovieDetailsMapper
) : ViewModel() {

    private val _movieStateFlow = MutableStateFlow<UIMovieStateFlow>(UIMovieStateFlow.Empty)
    val movieStateFlow : StateFlow<UIMovieStateFlow> = _movieStateFlow

    init {
        viewModelScope.launch {
            try {
                _movieStateFlow.value = UIMovieStateFlow.Loading(true)
                val movieDetailResponse = getMovieDetails.getMovieDetails()

                val movieDetails = movieDetailsMapper.mapFromEntity(movieDetailResponse)
                _movieStateFlow.value = UIMovieStateFlow.Success(movieDetails)

            } catch (throwable: Throwable) {
                Timber.e(throwable)
            } finally {
                _movieStateFlow.value = UIMovieStateFlow.Loading(false)
            }
        }
    }
}