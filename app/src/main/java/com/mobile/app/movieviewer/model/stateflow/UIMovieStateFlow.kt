package com.mobile.app.movieviewer.model.stateflow

import com.mobile.app.movieviewer.model.model.MovieDetails

sealed class UIMovieStateFlow {
    data class Error(val title: String, val errorMessage: String) : UIMovieStateFlow()
    data class Loading(val isLoading: Boolean) : UIMovieStateFlow()
    data class Success(val movieDetails: MovieDetails) : UIMovieStateFlow()
    object Empty : UIMovieStateFlow()
}
