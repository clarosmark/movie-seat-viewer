package com.mobile.app.movieviewer.model.entity

data class CinemaX(
    val cinema_id: String,
    val id: String,
    val label: String
)