package com.mobile.app.movieviewer.model.entity

data class Available(
    val seat_count: Int,
    val seats: List<String>
)