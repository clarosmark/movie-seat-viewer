package com.mobile.app.movieviewer.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mobile.app.movieviewer.model.entity.Cinema
import com.mobile.app.movieviewer.model.entity.Date
import com.mobile.app.movieviewer.model.entity.Time
import com.mobile.app.movieviewer.model.stateflow.DateStateFlow
import com.mobile.app.movieviewer.model.stateflow.UIMovieSeatMapFlow
import com.mobile.app.movieviewer.usecase.GetMovieDetails
import com.mobile.app.movieviewer.usecase.MovieDetailsMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieDetails: GetMovieDetails,
    private val movieDetailsMapper: MovieDetailsMapper
) : ViewModel() {

    private val _seatMapFlow = MutableStateFlow<UIMovieSeatMapFlow>(UIMovieSeatMapFlow.Empty)
    val seatMapFlow: StateFlow<UIMovieSeatMapFlow> = _seatMapFlow

    private val _dateStateFlow = MutableStateFlow<DateStateFlow>(DateStateFlow.DateSchedule(listOf()))
    val dateStateFlow: StateFlow<DateStateFlow> = _dateStateFlow



    private var dateList = listOf<Date>()
    private var cinemaList = listOf<Cinema>()
    private var timeList = listOf<Time>()


    init {
        viewModelScope.launch {
            try {
                _seatMapFlow.value = UIMovieSeatMapFlow.Loading(true)

                val movieScheduleResponse = async {
                    getMovieDetails.getMovieSchedules()
                }

                val movieSeatResponse = async {
                    getMovieDetails.getMovieSeatMap()
                }

                val data = movieScheduleResponse.await()
                val seatData = movieSeatResponse.await()

                dateList = data.dates
//                cinemaList = data.cinemas
//                timeList = data.times
//
                val dateValue = dateList.map { it.label }
                _dateStateFlow.value = DateStateFlow.DateSchedule(dateValue)
//                _seatMapFlow.value = UIMovieSeatMapFlow.DateSchedule(dateValue)



            } catch (throwable: Throwable) {
                Timber.e(throwable)
            } finally {
                _seatMapFlow.value = UIMovieSeatMapFlow.Loading(false)
            }
        }
    }


}